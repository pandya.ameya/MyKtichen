import React, { Component } from 'react';
import {
  AppRegistry,
  Navigator,
  View,
  StatusBar
} from 'react-native';

// import {SplashPage} from './components/SplashPage';
const SplashPage = require('./components/SplashPage');
const LoginPage = require('./components/LoginPage');
const RegisterPage = require('./components/RegisterPage');

export default class MyKitchen extends Component {
    renderScene ( route, navigator ) {
        var routeId = route.id;
        if (routeId === 'SplashPage') {
            return (
                <SplashPage
                    navigator={navigator}/>
            );
        }
        if (routeId === 'LoginPage') {
            return (
                <LoginPage
                    navigator={navigator}/>
            );
        }
        if (routeId === 'RegisterPage') {
            return (
                <RegisterPage
                    navigator={navigator}/>
            );
        }
    }

    render() {
        return (
            <View style={{flex:1}}>
                <StatusBar
                    backgroundColor='#e0e0e0'>
                </StatusBar>
                <Navigator
                    ref={(nav) => { navigator = nav; }}
                    initialRoute = {{id: 'SplashPage', name: 'Index'}}
                    renderScene = {this.renderScene.bind(this)}
                    configureScene = {(route) => {
                        if (route.sceneConfig) {
                            return route.sceneConfig;
                        }
                        return Navigator.SceneConfigs.FadeAndroid;
                    }}/>
            </View>
        );
    }
}

AppRegistry.registerComponent('MyKitchen', () => MyKitchen);
