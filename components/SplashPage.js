import React, { Component } from 'react';
import {
    StyleSheet,
    StatusBar,
    Text,
    View,
    Button,
    TouchableHighlight,
    TouchableOpacity
} from 'react-native';

import { Icon } from 'react-native-material-design';
// import Icon from 'react-native-vector-icons/MaterialIcons';
// import { IconToggle } from 'react-native-material-design';

class SplashPage extends Component {
    componentWillMount () {
        var navigator = this.props.navigator;
        setTimeout (() => {
            navigator.replace({
                id: 'LoginPage',
            });
        }, 5000);
    }

    render () {
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor='#e0e0e0'>
                </StatusBar>

                <View style={styles.main}>
                    <Text style={styles.brand}>
                        MyKitchen
                    </Text>
                </View>

                <TouchableOpacity
                    style={styles.fab}
                    onPress={()=>this.props.navigator.replace({id:'LoginPage'})}>

                    <Text>
                        <Icon
                            name="arrow-forward"
                            size={30}
                            color="#ffffff"/>
                    </Text>

                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        padding: 16,
        backgroundColor: '#f5f5f5',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        flexDirection: 'column'
    },
    main: {
        flex: 8,
        justifyContent: 'center'
    },
    brand: {
        color: 'black',
        fontSize: 48,
    },
    fab: {
        backgroundColor: '#ff3f80',
        width: 54,
        height: 54,
        borderRadius: 28,
        marginTop: 24,
        marginBottom: 56,
        justifyContent: 'center',
        alignItems: 'center',
        elevation: 10
    }
});

module.exports = SplashPage;
