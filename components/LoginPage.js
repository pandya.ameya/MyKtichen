import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    Button,
    TouchableHighlight,
    TouchableOpacity,
    View,
    Navigator,
    BackAndroid
} from 'react-native';

// import { Button } from 'react-native-material-design';
import { Divider } from 'react-native-material-design';

class LoginPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: ''
        };
    }

    _login() {
        console.log(this.state.username, this.state.password);
    }

    render () {
        BackAndroid.addEventListener('hardwareBackPress', function() {
            if (navigator && navigator.getCurrentRoutes().length > 1) {
                navigator.pop();
                return true;
            }
            return false;
        });
        return (
            <View style={styles.container}>
                <View style={styles.loginWindow}>
                    <Text style={styles.title}>Login</Text>
                    <Divider />
                    <TextInput
                        style={styles.textinput1}
                        placeholder='Username'
                        keyboardType='email-address'
                        onChangeText={(text)=>{
                            this.state.username = text
                        }}
                    />
                    <TextInput
                        style={styles.textinput2}
                        placeholder='Password'
                        onChangeText={(text)=>{
                            this.state.password = text
                        }}
                        secureTextEntry={true}/>
                    <Button
                        style={styles.button}
                        title='Login'
                        color='#07f'
                        onPress={this._login}/>
                </View>
                <View style={styles.footer}>
                    <Text style={styles.disabled}>Not a member?</Text>
                    <TouchableOpacity>
                        <Text
                            style={styles.hyperlink}
                            onPress={()=>{this.props.navigator.push({id:'RegisterPage'})}}>Register</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        padding: 16,
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    loginWindow: {
        alignSelf: 'stretch',
        padding: 16,
        backgroundColor: 'white',
        elevation: 3,
        marginBottom: 16,
        borderRadius: 3
    },
    title: {
        fontSize: 36,
        justifyContent: 'center',
        paddingBottom: 16,
        alignSelf: 'center'
    },
    textinput1: {
        fontSize: 20,
        marginBottom: 16
    },
    textinput2: {
        fontSize: 20,
        marginBottom: 16
    },
    button: {
        fontSize: 20,
        padding: 15,
    },
    footer: {
        padding: 16,
        alignItems: 'center'
    },
    disabled: {
        color: '#444'
    },
    hyperlink: {
        fontSize: 16,
        color: '#07f',
        padding: 8,
    }
});

module.exports = LoginPage;
