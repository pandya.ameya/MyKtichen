// To send an AJAX request, just add the following code in the function
// `_login` in LoginPage.js and `_register` in RegisterPage.js

fetch('https://mywebsite.com/endpoint/', {
  method: 'POST',
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
  },
  body: JSON.stringify({
    formdata: {
        // get the username, password and other data from the state
        // for eg.
        username: this.state.username,
        password: this.state.password
    }
  })
})
.then((response) => response.json())
.then((responseJson) => {
    if (responseJson.error) {
        throw responseJson.error;
    }
    return responseJson;
})
.catch((error) => {
    console.error(error);
});
// for more info: https://facebook.github.io/react-native/docs/network.html


// this method returns the result from the .then() method.
// based on the result, the page can be navigated forward using navigator, eg.
navigator.push({id: 'Next page id'});
// but make sure the route for the page id is added in the
// renderScene() method in index.android.js
// for more info:
// StackNavigator: https://facebook.github.io/react-native/docs/navigation.html
// or Navigator: https://facebook.github.io/react-native/docs/navigator.html
