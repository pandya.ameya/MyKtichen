import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    Button,
    TouchableHighlight,
    TouchableOpacity,
    View,
    Navigator,
    BackAndroid
} from 'react-native';

// import { Button } from 'react-native-material-design';
import { Divider } from 'react-native-material-design';

class LoginPage extends Component {

    _register() {
        console.log(this.state.name, this.state.email, this.state.password, this.state.cnfPassword);
    }

    render () {
        BackAndroid.addEventListener('hardwareBackPress', function() {
            if (navigator && navigator.getCurrentRoutes().length > 1) {
                navigator.pop();
                return true;
            }
            return false;
        });
        return (
            <View style={styles.container}>
                <View style={styles.loginWindow}>
                    <Text style={styles.title}>Register</Text>
                    <Divider />
                    <TextInput
                        style={styles.textinput}
                        placeholder='Name'
                        onChangeText={(text)=>{
                            this.state.name = text
                        }}/>
                    <TextInput
                        style={styles.textinput}
                        placeholder='Email Address'
                        keyboardType='email-address'
                        onChangeText={(text)=>{
                            this.state.email = text
                        }}/>
                    <TextInput
                        style={styles.textinput}
                        placeholder='Password'
                        onChangeText={(text)=>{
                            this.state.password = text
                        }}
                        secureTextEntry={true}/>
                    <TextInput
                        style={styles.textinput}
                        placeholder='Confirm Password'
                        onChangeText={(text)=>{
                            this.state.cnfPassword = text
                        }}
                        secureTextEntry={true}/>
                    <Button
                        style={styles.button}
                        title='Login'
                        color='#07f'
                        onPress={this._register}/>
                </View>
                <View style={styles.footer}>
                    <Text style={styles.hint}>Already a member?</Text>
                    <TouchableOpacity>
                        <Text
                            style={styles.hyperlink}
                            onPress={()=>{this.props.navigator.pop()}}>Sign In</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        padding: 16,
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    loginWindow: {
        alignSelf: 'stretch',
        padding: 16,
        backgroundColor: 'white',
        elevation: 3,
        marginBottom: 16,
        borderRadius: 3
    },
    title: {
        fontSize: 36,
        justifyContent: 'center',
        paddingBottom: 16,
        alignSelf: 'center'
    },
    textinput: {
        fontSize: 20,
        marginBottom: 16
    },
    button: {
        fontSize: 20,
        padding: 15,
    },
    footer: {
        padding: 16,
        alignItems: 'center'
    },
    hint: {
        color: '#444'
    },
    hyperlink: {
        fontSize: 16,
        color: '#07f',
        padding: 8,
    }
});

module.exports = LoginPage;
